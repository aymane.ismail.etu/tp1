const name = "Regina";
var url = "images/" + name.toLowerCase() + ".jpg" ;
//let html = '<a href=\"' + url +'\">' + url + '</a>';
/*let html = '<a href=\"' + url +'\">\n' +
'<img src=\"'+url+'\"/>\n'+
'<section>Regina</section>' + '</a>';*/


//const data = ['Regina', 'Napolitaine', 'Spicy'];

/*
let html="";

data.forEach(element => {
    
    url = "images/" + element.toLowerCase() + ".jpg" ;

    let html2 = ` <article class="pizzaThumbnail"> 
    <a href="${url}">
    <img src=${url}>
    <section>${element}</section>
    </a>
    </article>`  ;

    console.log(html);

    html += html2;

}       
);
*/

let data = [
	{
		name: 'Regina',
		base: 'tomate',
		price_small: 6.5,
		price_large: 9.95,
		image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
	},
	{
		name: 'Napolitaine',
		base: 'tomate',
		price_small: 6.5,
		price_large: 8.95,
		image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
	},
	{
		name: 'Spicy',
		base: 'crème',
		price_small: 5.5,
		price_large: 8,
		image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300',
	}
];



// trie par nom
data.sort((a, b) => a.name.localeCompare(b.name))
console.log(data);

data.sort((a, b) => a.price_small - b.price_small)
console.log(data);

//data = data.filter(word => word.base == 'tomate' );

//data = data.filter(word => word.price_small < 6 );

data = data.filter(word => word.name == 2);


let html="";

data.forEach(element => {
    
  

    let html2 = `<article class="pizzaThumbnail">
	<a href="${element.image}">
		<img src="${element.image}" />
		<section>
			<h4>${element.name}</h4>
			<ul>
				<li>Prix petit format : ${element.price_small} €</li>
				<li>Prix grand format : ${element.price_large} €</li>
			</ul>
		</section>
	</a>
</article>`;

    html += html2;

}       
);

document.querySelector('.pageContent').innerHTML = html;

//console.log(html);